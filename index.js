//directions:
//create 2 event listeners to an event when a user types in the first and last name inputs
//when this event triggers, update then span-full-names content to show the value of the first anme input on the left and the vaue of the last name input on the right

// stretch goal: instead of an anonymous function, create a new function that two event listers will call

// guide question: where do the names come from and where should they go?


let input1= document.querySelector('#txt-first-name')
let input2= document.querySelector('#txt-last-name')
let fullName=document.querySelector('#span-full-name') 

input1.addEventListener('keyup',(e)=>{
	console.log(e.target.value)
fullName.innerHTML = (`${input1.value} ${input2.value}`)	
})

input2.addEventListener('keyup',(e)=>{
	console.log(e.target.value)
	console.dir(e.target.value)
	fullName.innerHTML = (`${input1.value} ${input2.value}`)
})

